# Полезные ссылки:

- [Документация по настройке и тестированию Альфа-линк модуль 1С:Директбанк](https://github.com/alfa-laboratory/alfalink-1c-directbank-documentation)
- [Настройка OpenSSL и экспортирование в формате PKCS#12 из криптоконтейнера](https://www.cryptopro.ru/forum2/default.aspx?g=posts&m=63478)
- [FAQ - Linux/Unix](https://www.cryptopro.ru/category/faq/linuxunix-0)
- [Установка КриптоПро CSP на ОС Linux + Rutoken](https://support.cryptopro.ru/index.php?/Knowledgebase/Article/View/87/2/ustnovk-kriptopro-csp-n-os-linux--rutoken)
- [устанорвка .pfx сертификата в *nix системах](https://support.cryptopro.ru/index.php?/Knowledgebase/Article/View/229/0/rbot-s-pfx-v-nix-sistemkh.)
- [Расширение для Python](https://docs.cryptopro.ru/cades/pycades)

# Конвертация ключа PFX -> PEM
https://habr.com/ru/post/353586/
https://dzek.ru/blog/163.html
https://www.cryptopro.ru/forum2/default.aspx?g=posts&m=63478
https://stackoverflow.com/questions/15413646/converting-pfx-to-pem-using-openssl

#  Может понадобится
https://gist.github.com/ninovsnino/8b0e5ce773b959da6c16
https://stackoverflow.com/questions/32505722/signing-data-using-openssl-with-python
https://stackoverflow.com/questions/33528476/python-subprocess-with-openssl
https://chromium.googlesource.com/chromium/src/+/master/net/tools/print_certificates.py