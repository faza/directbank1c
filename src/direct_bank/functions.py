import datetime
import typing

from direct_bank.request.packet import PacketRequest
from direct_bank.response.result_bank import SendPacketResponse
from direct_bank.document.base import BaseDocument
from direct_bank.document.statements import StatementRequest
from direct_bank.api import DirectBankAPI


def get_session_id() -> str:
    """
    Аутентификация по логину и паролю(одно факторная аутентификация),
    с получением идентификатора авторизованной сессии
    """
    resp = DirectBankAPI.logon()
    return resp.sid


def send_statements_request(sid: str,
                            date_from: datetime.datetime,
                            date_to: datetime.datetime,
                            statement_type: typing.Optional[int] = None) -> SendPacketResponse:
    """
    Получение выписки банка

    :param sid: идентификатор авторизованной сессии
    :param date_from: начало периода формирования выписки
    :param date_to: конец периода формирования выписк
    :param statement_type: Тип выписки: 0 - Окончательная выписка;
                                        1 - Промежуточная выписка;
                                        2 - Текущий остаток на счете

    :return: ответ банка
    """
    # Запрос на получение выписки банка
    statement_request = StatementRequest()
    statement_request.make_xml(date_from, date_to, statement_type)
    return send_packet(sid=sid, request=statement_request)


def send_packet(sid: str, request: BaseDocument) -> SendPacketResponse:
    """
    Отправка транспортного контейнера

    :param sid: идентификатор авторизованной сессии
    :param request: запрос
    :return: ответ банка
    """
    packet_request = PacketRequest()
    response = DirectBankAPI.send_pack(sid=sid, data=packet_request.make_xml(request))
    return response


def get_packets(sid: str, date_from: datetime.datetime) -> list:
    """
    Получение списка транспортных контейнеров

    :param sid: идентификатор авторизованной сессии
    :param date_from: момент времени, с которого будет формироваться список идентификаторов контейнеров,
                      подготовленных к передаче в хронологическом порядке.
    :return: список идентификаторов контейнеров
    """
    resp = DirectBankAPI.get_pack_list(sid=sid, date=date_from)
    return resp.packets


def get_packet_data(sid: str, guid: str):
    """
    Получения транспортного контейнера по его идентификатору

    :param sid: идентификатор авторизованной сессии
    :param guid: идентификатор транспортного контейнера
    :return: результ, либо статус обработки пакета
    """
    resp = DirectBankAPI.get_pack(sid=sid, guid=guid)
    return resp
