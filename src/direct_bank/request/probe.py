import uuid
import datetime

from direct_bank.core import FORMAT_DATE_TIME
from direct_bank.document.base import BaseDocument
from direct_bank.schemas.probe_schema import ProbeXMLSchema


class Probe(BaseDocument):
    class_schema = ProbeXMLSchema
    dock_ind = '05'

    def prepare_json_data(self) -> dict:
        # Notice: Похоже что порядок ключей важен!!!
        json_data = {
            '@xmlns': 'http://directbank.1c.ru/XMLSchema',
            '@xmlns:xs': 'http://www.w3.org/2001/XMLSchema',
            '@xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
            '@creationDate': datetime.datetime.now().strftime(FORMAT_DATE_TIME),
            '@formatVersion': self.format_version,
            '@id': str(uuid.uuid4()),
            '@userAgent': self.conf.user_agent,
            'Sender': {
                '@id': str(self.conf.customer_id),  # TODO: Что то вроде не то, помоему есть некий идентификатор клиента!!!
                '@inn': str(self.conf.customer_inn),
                '@name': self.conf.customer_name,
                '@kpp': str(self.conf.customer_kpp),
            },
            'Recipient': {
                    '@bic': self.conf.bank_bic,
                    '@name': self.conf.bank_name,
            },
        }
        return json_data
