import uuid
import datetime

from direct_bank.core import FORMAT_DATE_TIME
from direct_bank.document.base import BaseDocument
from direct_bank.schemas.packet_schema import PacketXMLSchema


class PacketRequest(BaseDocument):
    """Транспортный контейнер"""
    class_schema = PacketXMLSchema

    def prepare_json_data(self, payload: BaseDocument, need_signature: bool = False) -> dict:
        # Notice: Похоже что порядок ключей важен!!!
        json_data = {
            '@xmlns': 'http://directbank.1c.ru/XMLSchema',
            '@xmlns:xs': 'http://www.w3.org/2001/XMLSchema',
            '@xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
            '@creationDate': datetime.datetime.now().strftime(FORMAT_DATE_TIME),
            '@formatVersion': self.format_version,
            '@id': str(uuid.uuid4()),
            '@userAgent': self.conf.user_agent,
            'Sender': {
                'Customer': {
                    '@id': str(self.conf.customer_id),  # TODO: Что то вроде не то, помоему есть некий идентификатор клиента!!!
                    '@inn': str(self.conf.customer_inn),
                    '@name': self.conf.customer_name,
                }
            },
            'Recipient': {
                'Bank': {
                    '@bic': self.conf.bank_bic,
                    '@name': self.conf.bank_name,
                }
            },
            'Document': {
                '@dockind': str(payload.dock_ind),
                '@formatVersion': self.format_version,
                '@id': str(uuid.uuid4()),
                'Data': payload.b64encode,
            }
        }
        if need_signature:
            signed_data, issuer, serial_number = payload.sign_data()
            json_data['Document'].update({
                'Signature': {
                    '@x509IssuerName': issuer,
                    '@x509SerialNumber': serial_number,
                    'SignedData': signed_data,
                }
            })
        return json_data
