from direct_bank.schemas.settings_schema import SettingsXMLSchema


DEFAULT_USER_AGENT = '1C+Enterprise/8.3'


class ConfigurationError(Exception):
    pass


class Configuration(object):
    """
    Класс, представляющий конфигурацию.
    """
    login = None
    password = None
    api_url = None
    customer_id = None
    customer_inn = None
    customer_kpp = None
    customer_name = None

    bank_bic = None
    bank_name = None

    user_agent = None
    timeout = 1800
    verify_request = True
    key_file = None
    coder = None

    def __init__(self, **kwargs):
        self.assert_has_api_credentials()

    @staticmethod
    def configure(passwd=None,
                  login=None,
                  customer_id=None,
                  customer_inn=None,
                  customer_kpp=None,
                  customer_name=None,
                  bank_bic=None,
                  bank_name=None,
                  api_url=None,
                  format_version=None,
                  xml=None,
                  key_file=None, **kwargs) -> None:
        Configuration.password = passwd
        if xml:
            schema_xml = SettingsXMLSchema(source=xml)
            if not schema_xml.is_valid():
                raise ConfigurationError('Не верная структура XML файла настроек.')

            login = login or schema_xml.get_data('Data.Logon.Login.User')
            customer_id = customer_id or schema_xml.get_data('Data.CustomerID')
            customer_inn = customer_inn or schema_xml.get_data('Recipient.@inn')
            customer_kpp = customer_kpp or schema_xml.get_data('Recipient.@kpp')
            customer_name = customer_name or schema_xml.get_data('Recipient.@name')
            bank_bic = bank_bic or schema_xml.get_data('Sender.@bic')
            bank_name = bank_name or schema_xml.get_data('Sender.@name')
            api_url = api_url or schema_xml.get_data('Data.BankServerAddress')
            format_version = format_version or schema_xml.get_data('Data.FormatVersion')

        Configuration.login = login
        Configuration.customer_id = customer_id
        Configuration.customer_inn = customer_inn
        Configuration.customer_kpp = customer_kpp
        Configuration.customer_name = customer_name
        Configuration.bank_bic = bank_bic
        Configuration.bank_name = bank_name
        Configuration.api_url = api_url
        Configuration.format_version = format_version
        Configuration.user_agent = kwargs.get('user_agent', DEFAULT_USER_AGENT)
        Configuration.timeout = kwargs.get("timeout", 1800)
        Configuration.verify_request = kwargs.get("verify_request", True)
        Configuration.key_file = key_file
        Configuration.coder = kwargs.get("coder", 'direct_bank.crypto.open_ssl.OpenSSLCoder')

    @staticmethod
    def instantiate() -> 'Configuration':
        """Получение объекта конфигурации"""
        return Configuration(
            password=Configuration.password,
            login=Configuration.login,
            customer_id=Configuration.customer_id,
            customer_inn=Configuration.customer_inn,
            customer_kpp=Configuration.customer_kpp,
            customer_name=Configuration.customer_name,
            bank_bic=Configuration.bank_bic,
            bank_name=Configuration.bank_name,
            api_url=Configuration.api_url,
            user_agent=Configuration.user_agent,
            timeout=Configuration.timeout,
            verify_request=Configuration.verify_request,
            coder=Configuration.coder,
        )

    def assert_has_api_credentials(self) -> None:
        if self.password is None:
            raise ConfigurationError("password are required")
