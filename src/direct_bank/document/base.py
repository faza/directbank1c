import json
import base64
import typing
import xmlschema

from direct_bank.configuration import Configuration
from direct_bank.utils import import_class_of_module


class BaseDocument(object):
    class_schema: xmlschema.XMLSchema = None
    format_version = '2.2.2'
    result_xml = None
    coder_class = None

    def __init__(self):
        self.conf = Configuration.instantiate()
        self.schema = self.class_schema()
        if self.conf.key_file:
            self.coder_class = import_class_of_module(self.conf.coder)(self.conf.key_file)

    def prepare_json_data(self, *args) -> dict:
        raise NotImplementedError('Метод не определён')

    def make_xml(self, *args) -> bytes:
        if self.result_xml is None:
            nsmap = {
                '': 'http://directbank.1c.ru/XMLSchema',
                'xs': 'http://www.w3.org/2001/XMLSchema',
                'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
            }
            json_data = self.prepare_json_data(*args)
            data = json.dumps(json_data)
            xml = xmlschema.from_json(data, schema=self.schema.schema, preserve_root=True)
            result = xmlschema.etree.etree_tostring(xml, xml_declaration=True, encoding='UTF-8', namespaces=nsmap)

            # Валидация полученного XML
            if not self.schema.schema.is_valid(result.decode()):
                raise Exception('Создаваемый XML неверен, не соответствует xsd-схеме.')

            self.result_xml = result
        return self.result_xml

    @property
    def b64encode(self) -> str:
        """Метод преобразующий результирующий xml в строку base64"""
        if self.result_xml is None:
            raise Exception('Сначала необходимо вызвать метод `.make_xml`')
        return base64.b64encode(self.result_xml).decode()

    def sign_data(self) -> typing.Tuple[str, str, str]:
        """
        Метод возвращающий шифрованный(подписанный) результирующий xml и данные сертификата
        :return: (подписанный xml, имя издателя сертификата, серийный номер сертификата)
        """
        if not self.coder_class:
            raise Exception('Кодировщик не был указан')

        # if self.result_xml  TODO: Сделать проверку

        encoded_data = self.coder_class.encrypt(self.b64encode)
        issuer = self.coder_class.issuer
        serial_number = self.coder_class.serial_number
        return encoded_data, issuer, serial_number
