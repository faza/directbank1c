import uuid
import typing
import datetime

from direct_bank.core import FORMAT_DATE_TIME
from direct_bank.document.base import BaseDocument
from direct_bank.schemas.request_statements_schema import StatementRequestXMLSchema


class StatementRequest(BaseDocument):
    """Запрос выписки банка"""
    class_schema = StatementRequestXMLSchema
    dock_ind = '14'

    def prepare_json_data(self,
                          date_from: datetime.datetime,
                          date_to: datetime.datetime,
                          statement_type: typing.Optional[int] = None) -> dict:
        # Notice: Похоже что порядок ключей важен!!!
        json_data = {
            '@xmlns': 'http://directbank.1c.ru/XMLSchema',
            '@xmlns:xs': 'http://www.w3.org/2001/XMLSchema',
            '@xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
            '@creationDate': datetime.datetime.now().strftime(FORMAT_DATE_TIME),
            '@formatVersion': self.format_version,
            '@id': str(uuid.uuid4()),
            '@userAgent': self.conf.user_agent,
            'Sender': {
                '@id': str(self.conf.customer_id),
                '@name': self.conf.customer_name,
                '@inn': str(self.conf.customer_inn),
                '@kpp': str(self.conf.customer_kpp),
            },
            'Recipient': {
                '@bic': self.conf.bank_bic,
                '@name': self.conf.bank_name
            },
            'Data': {
                'StatementType': str(statement_type or 0),
                'DateFrom': date_from.strftime(FORMAT_DATE_TIME),
                'DateTo': date_to.strftime(FORMAT_DATE_TIME),
                'Account': str(self.conf.customer_id),
                'Bank': {
                    'BIC': self.conf.bank_bic,
                    'Name': self.conf.bank_name,
                },
            },
        }
        return json_data
