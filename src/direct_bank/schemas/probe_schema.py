from direct_bank.schemas.base import XMLSchema


class ProbeXMLSchema(XMLSchema):
    xsd_file_name = '1C-Bank_Probe.xsd'
