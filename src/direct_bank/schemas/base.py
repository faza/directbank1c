import os
import typing
import xmlschema

from direct_bank.utils import get_value_of_dict


class XMLSchema(object):
    """
    Базовый класс работы с XML-схемами данных
    """
    base_xsd_path = 'xsd'
    xsd_file_name = None
    schema = None
    source = None
    _dict_data = None

    def __init__(self, source=None) -> None:
        self._dict_data = None
        rel_dir = os.path.dirname(os.path.abspath(__file__))
        xsd_file_path = os.path.join(rel_dir, self.base_xsd_path, self.xsd_file_name)
        self.schema = xmlschema.XMLSchema(source=xsd_file_path)
        self.source = source

    def is_valid(self) -> bool:
        """
        Метод проверки источника данных, на соответствие структуре данных,
        описанных в `self.xsd_file_name`.

        Возвращает True, если данные XML действительны,
                   False, если они недействительны.
        """
        return self.schema.is_valid(self.source)

    def get_data(self, key: str) -> typing.Any:
        """
        Получение значения из инициализированных данных по `ключу`
        :param key: ключ
        :return: значение найденное по ключу
        """
        return get_value_of_dict(data=self.dict_data, key=key)

    @property
    def dict_data(self) -> dict:
        """
        Декодирование данных к массиву ключ-значение
        :return: dict - массив преобразованных данных
        """
        if self._dict_data is None:
            # TODO: Проверка на валидность источника, в случае необходимости возбуждение ошибки !??
            self._dict_data = self.schema.to_dict(self.source)
        return self._dict_data
