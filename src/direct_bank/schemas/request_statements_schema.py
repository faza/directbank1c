from direct_bank.schemas.base import XMLSchema


class StatementRequestXMLSchema(XMLSchema):
    """XML-схема запроса выписки"""
    xsd_file_name = '1C-Bank_StatementRequest.xsd'
