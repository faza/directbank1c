from direct_bank.schemas.base import XMLSchema


class PacketXMLSchema(XMLSchema):
    """XML-схема транспортного контейнера"""
    xsd_file_name = '1C-Bank_Packet.xsd'
