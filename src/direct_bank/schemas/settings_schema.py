from direct_bank.schemas.base import XMLSchema


class SettingsXMLSchema(XMLSchema):
    """XML-схема настроек обмена с банком"""
    xsd_file_name = '1C-Bank_Settings.xsd'
