from direct_bank.schemas.base import XMLSchema


class StatusPacketNoticeXMLSchema(XMLSchema):
    """XML-схема извещения о состоянии обработки транспортного контейнера"""
    xsd_file_name = '1C-Bank_StatusPacketNotice.xsd'


class StatusDocNoticeXMLSchema(XMLSchema):
    """XML-схема извещения о состоянии электронного документа"""
    xsd_file_name = '1C-Bank_StatusDocNotice.xsd'


class StatementXMLSchema(XMLSchema):
    """XML-схема выписки банка"""
    xsd_file_name = '1C-Bank_Statement.xsd'
