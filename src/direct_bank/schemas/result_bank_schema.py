from direct_bank.schemas.base import XMLSchema


class ResultBankXMLSchema(XMLSchema):
    """XML-схема ответа банковского сервиса"""
    xsd_file_name = '1C-Bank_ResultBank.xsd'
