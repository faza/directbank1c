class BaseCoder(object):
    """
    Базовый объект кодировщика, для наследования.
    Все описанные в нём методы должны присутствовать в наследующих объектах
    """

    cert = None

    def decrypt(self, data: str) -> str:
        """Расшифровка данных"""
        raise NotImplementedError('Метод не определён')

    def encrypt(self, data: str) -> str:
        """Шифрование данных"""
        raise NotImplementedError('Метод не определён')

    @property
    def serial_number(self):
        """Серийный номер сертификата открытого ключа ЭП"""
        raise NotImplementedError('Метод не определён')

    @property
    def issuer(self):
        """Имя издателя сертификата открытого ключа ЭП (значение атрибута "CN")."""
        raise NotImplementedError('Метод не определён')
