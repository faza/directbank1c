import re
import typing
import pycades


class CryptoPROCoder(object):
    """
    Кодировщик, использующий библиотеку `pycades` от КриптоПРО.
    https://docs.cryptopro.ru/cades/pycades
    """

    cert: pycades.Certificate = None

    def __init__(self, serial_number: str):
        """
        При инициализации осуществляет поиск сертификата
        в хранилище персональных сертификатов пользователя по переданному номеру.
        В случае не обнаружения сертификата по номеру возбуждается ошибка.

        :param serial_number: серийный номер сертификата
        """
        store = pycades.Store()
        store.Open(pycades.CADESCOM_CONTAINER_STORE, pycades.CAPICOM_MY_STORE,
                   pycades.CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED)
        certs = store.Certificates
        if certs.Count == 0:
            raise Exception("В хранилище персональных сертификатов пользователя "
                            "нет ни одного сертификата с закрытым ключом")

        for index in range(1, certs.Count + 1):
            certificate = certs.Item(index)
            if certificate.SerialNumber == serial_number:
                self.cert = certificate

        if self.cert is None:
            raise Exception("Сертификат по серийному номеру не найден")

    def decrypt(self, data: str) -> str:
        """Расшифровка данных"""

    def encrypt(self, data: str) -> str:
        """Шифрование данных"""
        data = self._data_to_str(data)

        signer = pycades.Signer()
        signer.Certificate = self.cert
        signer.CheckCertificate = True

        enveloped_data = pycades.EnvelopedData()
        enveloped_data.Content = data
        enveloped_data.Recipients.Add(self.cert)
        signature = enveloped_data.Encrypt(pycades.CADESCOM_ENCODE_BASE64)

        signature = signature.replace('\r\n', '')
        return signature

    @property
    def serial_number(self):
        """Серийный номер сертификата открытого ключа ЭП"""
        return self.cert.SerialNumber

    @property
    def issuer(self):
        """Имя издателя сертификата открытого ключа ЭП (значение атрибута "CN")."""
        issuer_data = re.search('CN=(.+?),', self.cert.IssuerName)
        if issuer_data:
            return issuer_data.group(1)
        return ''

    @staticmethod
    def _data_to_str(data: typing.Union[str, bytes]):
        if isinstance(data, bytes):
            data = data.decode()
        return data
