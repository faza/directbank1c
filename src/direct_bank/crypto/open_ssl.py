import subprocess
import typing
from OpenSSL import crypto

from direct_bank.crypto.base import BaseCoder


class OpenSSLCoder(BaseCoder):
    """Кодировщик, использующий утилиту `openssl`."""

    def __init__(self, key_file: str):
        self.key_file = key_file
        self.cert = crypto.load_certificate(crypto.FILETYPE_PEM, open(self.key_file).read())

    def decrypt(self, data: str) -> str:
        """Расшифровка данных"""

    def encrypt(self, data: str) -> str:
        """Шифрование данных"""
        data = self._data_to_bytes(data)
        return self._execute_process(data).decode()

    @property
    def serial_number(self):
        """Серийный номер сертификата открытого ключа ЭП"""
        return f'{self.cert.get_serial_number():x}'

    @property
    def issuer(self):
        """Имя издателя сертификата открытого ключа ЭП (значение атрибута "CN")."""
        return str(self.cert.get_issuer())

    @staticmethod
    def _data_to_bytes(data: typing.Union[str, bytes]) -> bytes:
        if isinstance(data, str):
            data = data.encode()
        return data

    def _execute_process(self, data: bytes):
        """Метод исполняющий подписание документа"""
        cmd = f'openssl smime -sign -signer {self.key_file} -outform PEM'.split()
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
        result, stderr = proc.communicate(input=data)
        if proc.returncode != 0:
            raise Exception('The process was unsuccessful. Exception:', stderr)

        return result.replace(
            b'-----BEGIN PKCS7-----\n', b''
        ).replace(
            b'\n-----END PKCS7-----\n', b''
        ).replace(
            b'\n', b''
        )
