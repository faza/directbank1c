import ssl
import requests
import urllib3
import logging
from datetime import datetime
from urllib3.poolmanager import PoolManager
from urllib3.util import ssl_
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry

from direct_bank.configuration import Configuration


logger = logging.getLogger('direct_bank_api_client')

CIPHERS = (
    'ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384:'
    'ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-SHA256:AES256-SHA:TLS_AES_256_GCM_SHA384'
)


class TLSv12HttpAdapter(HTTPAdapter):
    """"Transport adapter" который позволяет использовать TLSv1_2."""

    def init_poolmanager(self, *pool_args, **pool_kwargs):
        self.poolmanager = PoolManager(*pool_args,
                                       ssl_version=ssl.PROTOCOL_TLSv1_2,
                                       **pool_kwargs)


class ApiClient:
    """Клиент выполнения запросов"""

    sid = None

    def __init__(self, configuration: Configuration, api_version: str = None):
        self.configuration = configuration
        self.api_version = api_version

    def request(self, method, path='', data='', headers=None, **kwargs):
        request_headers = self._prepare_request_headers(headers)
        raw_response = self.execute(method, path, data, request_headers, **kwargs)
        self._logging(data, raw_response)

        if raw_response.status_code != 200:
            self.__handle_error(raw_response=raw_response)

        if '/xml' not in raw_response.headers.get('Content-Type'):
            self.__handle_error(raw_response=raw_response)
            raise Exception('Ответ не содержит XML данные')

        return raw_response

    def execute(self, method, path, data, request_headers, **kwargs):
        session = self.get_session()
        raw_response = session.request(method,
                                       self.configuration.api_url + path,
                                       data=data,
                                       headers=request_headers,
                                       timeout=self.configuration.timeout / 1000,
                                       verify=self.configuration.verify_request,
                                       **kwargs)
        return raw_response

    def get_session(self):
        retry_strategy = Retry(
            connect=2,
            total=2,
            status_forcelist=[500, 502],
            backoff_factor=2
        )
        session = requests.Session()
        adapter = TLSv12HttpAdapter(max_retries=retry_strategy)
        session.mount("https://", adapter)
        return session

    def _prepare_request_headers(self, headers: dict):
        """Метод для подготовки заголовков запроса"""
        parsed_url = urllib3.util.parse_url(self.configuration.api_url)
        request_headers = {
            'Content-type': 'application/xml; charset=utf-8',
            'Host': parsed_url.hostname,
            'Accept': '*/*',
            'User-Agent': self.configuration.user_agent,
            'APIVersion': self.api_version,
            'AvailableAPIVersion': self.api_version,
        }
        if isinstance(headers, dict):
            request_headers.update(headers)
        return request_headers

    def __handle_error(self, raw_response):
        http_code = raw_response.status_code
        print(http_code)
        print(raw_response)
        # TODO: Возбуждение ошибок

    @staticmethod
    def _logging(data, response):
        """Метод для логирования запросов"""
        message = (
            '=================\n'
            f'{datetime.now()}\n'
            '------------------\n'
            f'request `{response.request.method}`: {response.request.url}\n{response.request.headers}\n'
            f'data:\n{data}\n'
            '------------------\n'
            f'response {response.status_code}: {response.content}\n'
            '=================\n'
        )
        logger.info(message)
