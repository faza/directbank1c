import xmlschema
from requests.models import Response

from direct_bank.schemas.result_bank_schema import ResultBankXMLSchema


class BaseBankResponse(object):
    """Базовый класс ответа банка"""
    class_schema: xmlschema.XMLSchema = None
    text = None
    _error = None

    def __init__(self, text: str):
        self._dict_data = None  # TODO: По моему не используется
        self.text = text
        self.schema = self.class_schema(self.text)
        self.schema.is_valid()
        self._check_error()

    def _check_error(self):
        self._error = self.schema.dict_data.get('Error')

    @property
    def error(self):
        if self._error:
            return {
                'code': self._error.get('Code'),
                'description': self._error.get('Description'),
                'info': self._error.get('MoreInfo'),
            }
        return None

    @property
    def dict_data(self):
        return self.schema.dict_data


class ResultBankResponse(BaseBankResponse):
    """
    Ответ банковского сервиса
    """
    class_schema = ResultBankXMLSchema

    def __init__(self, resp: Response):
        super(ResultBankResponse, self).__init__(resp.text)
