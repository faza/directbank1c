import base64
from decimal import Decimal

from direct_bank.response.base import ResultBankResponse, BaseBankResponse
from direct_bank.schemas.status_packet_notice import (
    StatusPacketNoticeXMLSchema,
    StatusDocNoticeXMLSchema,
    StatementXMLSchema,
)


class LoginResponse(ResultBankResponse):
    """Ответ банковского сервиса на метод Logon"""
    @property
    def sid(self):
        return self.schema.get_data('Success.LogonResponse.SID')


class PackListResponse(ResultBankResponse):
    """Ответ банковского сервиса на метод GetPackList"""
    @property
    def packets(self):
        return self.schema.get_data('Success.GetPacketListResponse.PacketID')


class PackResponse(ResultBankResponse):
    """Ответ банковского сервиса на метод GetPack"""
    @property
    def data(self):
        data = self.schema.get_data('Success.GetPacketResponse.Document.Data')
        data = base64.b64decode(data).decode()
        if 'StatusDocNotice' in data:
            return StatusDocNotice(data)
        if 'StatusPacketNotice' in data:
            return StatusPacketNotice(data)
        if 'Statement' in data:
            return Statement(data)


class SendPacketResponse(ResultBankResponse):
    """Ответ банковского сервиса на метод SendPack"""
    @property
    def id(self):
        return self.schema.get_data('Success.SendPacketResponse.ID')


class StatusPacketNotice(BaseBankResponse):
    """Извещение о состоянии обработки транспортного контейнера"""
    class_schema = StatusPacketNoticeXMLSchema


class StatusDocNotice(BaseBankResponse):
    """Извещение о состоянии электронного документа"""
    class_schema = StatusDocNoticeXMLSchema


class Statement(BaseBankResponse):
    """Выписка банка"""
    class_schema = StatementXMLSchema

    @property
    def opening_balance(self) -> Decimal:
        """Остаток на счете на начало периода"""
        return self.schema.get_data('Data.OpeningBalance')

    @property
    def closing_balance(self) -> Decimal:
        """Остаток на счете на конец периода"""
        return self.schema.get_data('Data.ClosingBalance')

    @property
    def operations(self) -> list:
        """Информация по операциям по лицевому счету в выписке"""
        operations = self.schema.get_data('Data.OperationInfo')
        return operations


