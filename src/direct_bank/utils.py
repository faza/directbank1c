import typing
from importlib import import_module


def import_class_of_module(dotted_path: str):
    """
    Импортирование класса из модуля по строке(модули и класс, разделяются точками)

    import_class_of_module('direct_bank.crypto.open_ssl.OpenSSLCoder')
    эквивалентно:
    from direct_bank.crypto.open_ssl import OpenSSLCoder

    :param dotted_path: - путь до импортируемого класса
    :return:
    """
    module_path, class_name = dotted_path.rsplit('.', 1)
    module = import_module(module_path)
    return getattr(module, class_name)


def get_value_of_dict(data: typing.Union[dict, list], key: str) -> typing.Any:
    """Получение значения из массива dict по ключу

    Ключ может иметь разделитель `.`, в этом случае значение будет получено рекурсивно
    Прим:
        >> data = {'item1': 'HI', 'item2': {'item_in': 333}, 'item3': [{'item_in3': 444}]}
        >> get_value(data, 'item1')
        'HI'
        >> get_value(data, 'item2.item_in')
        333
        >> get_value(data, 'item3.item_in3')
        444
    """
    split_key = key.split('.', 1)
    if isinstance(data, dict):
        value = data.get(split_key[0])
    elif isinstance(data, list):
        if len(data) == 1:
            value = data[0]
            return get_value_of_dict(value, key)
        raise ValueError('Не возможно получить данные по ключу!')
    else:
        raise ValueError('Неожиданный тип данных!')

    if len(split_key) == 1 or value is None:
        return value
    else:
        return get_value_of_dict(value, split_key[1])