import datetime
from requests.auth import HTTPBasicAuth

from direct_bank.configuration import Configuration
from direct_bank.client import ApiClient
from direct_bank.response.result_bank import (
    LoginResponse,
    PackListResponse,
    PackResponse,
    SendPacketResponse,
    StatusPacketNotice,
)


class DirectBankAPI:
    """
    API взаимодействия с банком
    """
    api_version = '2.2.2'

    def __init__(self):
        self.configuration = Configuration.instantiate()
        self.client = ApiClient(
            configuration=self.configuration,
            api_version=self.api_version
        )

    # TODO: Получение настроек обмена с банком в автоматическом режиме
    # def get_settings(cls):

    @classmethod
    def logon(cls):
        """
        Аутентификация по логину и паролю, однофакторная аутентификация
        Метод Logon (HTTP-метод POST)
        """
        instance = cls()
        path = '/Logon'
        headers = {'CustomerID': instance.configuration.customer_id}
        auth = HTTPBasicAuth(instance.configuration.login, instance.configuration.password)
        response = instance.client.request('POST', path, headers=headers, auth=auth)
        return LoginResponse(response)

    @classmethod
    def send_pack(cls, sid: str, data: str):
        """
        Отправка электронных документов
        Метод SendPack (HTTP-метод POST)
        """
        instance = cls()
        path = '/SendPack'
        headers = {
            'CustomerID': instance.configuration.customer_id,
            'SID': sid,
        }
        auth = HTTPBasicAuth(instance.configuration.login, instance.configuration.password)
        response = instance.client.request('POST', path, headers=headers, auth=auth, data=data)
        return SendPacketResponse(response)

    @classmethod
    def get_pack_list(cls, sid: str, date: datetime.datetime):
        """
        Получение списка GUID транспортных контейнеров, готовых к отправке клиенту банком
        Метод GetPackList (HTTP-метод GET)
        """
        instance = cls()
        path = '/GetPackList'
        headers = {
            'CustomerID': instance.configuration.customer_id,
            'SID': sid,
        }
        date = date.strftime('%d.%m.%Y %H:%M:%S')
        response = instance.client.request('GET', path, headers=headers, params={'date': date})
        return PackListResponse(response)

    @classmethod
    def get_pack(cls, sid: str, guid: str):
        """
        Получение транспортного контейнера по GUID из ранее полученного списка
        Метод GetPack (HTTP-метод GET)
        """
        instance = cls()
        path = '/GetPack'

        headers = {
            'CustomerID': instance.configuration.customer_id,
            'SID': sid,
        }
        response = instance.client.request('GET', path, headers=headers, params={'id': guid})
        if 'ResultBank' in response.text:
            return PackResponse(response)

        return StatusPacketNotice(response)
