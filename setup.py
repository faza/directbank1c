import setuptools

with open("VERSION") as version_file:
    version = version_file.read()

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="direct-bank",
    version=version,
    author="Alexandr Faizullin",
    author_email="faizullin.fan@gmail.com",
    description="Прямой обмен с банком по технологии системы \"1С:Предприятие 8\" (DirectBank)",
    long_description=long_description,
    long_description_content_type="text/markdown",
    # url="https://github.com/pypa/sampleproject",
    # project_urls={
    #     "Bug Tracker": "https://github.com/pypa/sampleproject/issues",
    # },
    install_requires=['urllib3', 'requests', 'lxml', 'xmlschema'],
    classifiers=[
        "Development Status :: 1 - Planning",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    package_data={
        "direct_bank": ["schemas/xsd/*.xsd"]
    },
    include_package_data=True,
    zip_safe=False,
    python_requires=">=3.6",
)
