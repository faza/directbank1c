from unittest import TestCase

from direct_bank.schemas.settings_schema import SettingsXMLSchema


class TestOpenXMLSchema(TestCase):
    def test_opening_settings_file(self):
        self.assertRaises(ValueError, SettingsXMLSchema)
        pass
